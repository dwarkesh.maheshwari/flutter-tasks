import 'package:flutter/material.dart';
import 'package:flutterapp/screens/users_profile_list.dart';
import 'screens/user_profile_detail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: UsersProfileList()
    );
  }
}

