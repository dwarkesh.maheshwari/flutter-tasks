import 'package:flutter/material.dart';
import 'package:flutterapp/models/user_profile.dart';
import 'package:flutterapp/utils/database_helper.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'background_paint.dart';

class UserDetailsForm extends StatefulWidget {
  final UserProfile userProfile;
  UserDetailsForm(this.userProfile);
  @override
  _UserDetailsFormState createState() => _UserDetailsFormState(this.userProfile);
}

class _UserDetailsFormState extends State<UserDetailsForm> {
  DatabaseHelper _databaseHelper = DatabaseHelper();

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _dateController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();
  TextEditingController _yearController = new TextEditingController();
  TextEditingController _dobController = new TextEditingController();
  String _name = '';
  String _date = '';
  int _age;
  String _latitude = '';
  String _longitude = '';
  List<String> dropdownValues = ["Male", "Female"];
  String selectedValue = '';
  UserProfile userProfile;

  _UserDetailsFormState(this.userProfile);

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void getUserLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.low);
    if (position != null) {
      setState(() {
        String lat = 'Latitude:- ' + position.latitude.toString();
        String lng = 'Longitude:- ' + position.longitude.toString();
        _latitude = lat;
        _longitude = lng;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController.text = userProfile.name;
    _dateController.text = userProfile.dob;
    _ageController.text = userProfile.age.toString();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomPaint(
        painter: MyPainter(),
        child: Center(
          child: Card(
            child: Container(
              margin: EdgeInsets.all(15),
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                        labelText: 'Name',
                      ),
                      validator: (value) {
                        if (value.trim().isEmpty) {
                          return 'Name is required!';
                        }
                      },
                    ),
                    TextFormField(
                      controller: _dateController,
                      decoration: InputDecoration(
                        labelText: 'Birthday',
                      ),
                      onTap: () {
                        setState(() {
                          _pickDate();
                        });
                      },
                      readOnly: true,
                    ),
/*                  TextFormField(
                      controller: _yearController,
                      decoration: InputDecoration(
                        hintText: 'Enter year',
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.trim().isEmpty ||
                            value.trim().length != 4 ||
                            int.parse(value.trim()) > DateTime.now().year) {
                          return 'Please enter a valid year!';
                        }
                      },
                      onChanged: (value) {
                        _dobController.text = '';
                      },
                    ),
                    TextFormField(
                      controller: _dobController,
                      decoration: InputDecoration(
                        hintText: 'Enter DOB 2',
                      ),
                      onTap: () {
                        if (formKey.currentState.validate()) {
                          pickDateOnTheBasisOfYear(
                              int.parse(_yearController.text) ?? 0);
                        }
                      },
                      readOnly: true,
                    ),*/
                    TextFormField(
                      controller: _ageController,
                      decoration: InputDecoration(
                        labelText: 'Age',
                      ),
                      readOnly: true,
                      validator: (value) {
                        if (value.isEmpty ||
                            int.parse(value) > 17 ||
                            int.parse(value) < 5) {
                          return 'Age must be greater than 4 and less than 18!';
                        }
                      },
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10.0),
                      child: DropdownButton(
                        items: dropdownValues.map((valueItem){
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem),
                          );
                        }).toList(),
                        value: getGenderFromInt(userProfile.gender),
                        onChanged: (newValue){
                          setState(() {
                            updateGenderAsInt(newValue);
                          });
                        },
                        isExpanded: true,
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        RaisedButton(
                          color: Colors.blue,
                          onPressed: () {
                            if (formKey.currentState.validate()) {
                              userProfile.name = _nameController.text;
                              userProfile.age = int.parse(_ageController.text);
                              userProfile.dob = _dateController.text;
                              _saveDataToDatabase();
//                            setDataToSharedPref();
                            }
                          },
                          child: Text(
                            'Save Data',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        RaisedButton(
                          color: Colors.deepOrangeAccent.shade100,
                          onPressed: () {
                            getUserLocation();
                          },
                          child: Text(
                            'Get Location',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        Text(_latitude),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text(_longitude),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _pickDate() async {
    DateTime pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1965),
      lastDate: DateTime.now(),
    );
    if (pickedDate != null) {
      setState(() {
        _date = DateFormat('dd-MMM-yyyy').format(pickedDate).toString();
        _dateController.text = _date;
        _calculateAge(pickedDate);
      });
    }
  }

  void pickDateOnTheBasisOfYear(int year) async {
    DateTime pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime(year - 17),
      firstDate: DateTime(year - 17),
      lastDate: DateTime.utc(year - 5, 12, 31),
    );
    if (pickedDate != null) {
      setState(() {
        String formattedDate =
        DateFormat('dd-MMM-yyyy').format(pickedDate).toString();
        _dobController.text = formattedDate;
      });
    }
  }

  void _calculateAge(DateTime pickedDate) {
    DateTime currentDate = DateTime.now();
    _age = currentDate.year - pickedDate.year;
    int month1 = currentDate.month;
    int month2 = pickedDate.month;
    if (month2 > month1) {
      _age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = pickedDate.day;
      if (day2 > day1) {
        _age--;
      }
    }
    _ageController.text = _age.toString();
  }

/*  getDataFromSharedPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _nameController.text = preferences.getString('name') ?? '';
    _dateController.text = preferences.getString('dob') ?? '';
    _yearController.text = preferences.getString('year') ?? '';
    _dobController.text = preferences.getString('dob_second') ?? '';
    int age = preferences.getInt('age') ?? 0;
    _ageController.text = age.toString();
  }

  setDataToSharedPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('name', _nameController.text);
    preferences.setString('dob', _dateController.text);
    preferences.setString('year', _yearController.text);
    preferences.setString('dob_second', _dobController.text);
    preferences.setInt('age', int.parse(_ageController.text) ?? 0);
    Fluttertoast.showToast(
        msg: 'Data Saved Successfully',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM);
  }*/

  void updateGenderAsInt(String value){
    switch (value){
      case 'Male':
        userProfile.gender = 1;
        break;
      case 'Female':
        userProfile.gender = 2;
        break;
      case 'Others':
        userProfile.gender = 3;
        break;
    }
  }

  String getGenderFromInt(int value){
    String gender;
    switch (value){
      case 1:
        gender = dropdownValues[0];
        break;
      case 2:
        gender = dropdownValues[1];
        break;
      case 3:
        gender = dropdownValues[2];
        break;
    }
    return gender;
  }

  void _saveDataToDatabase() async{
    int result;
    if(userProfile.id != null){
        result = await _databaseHelper.updateUserProfileInDatabase(userProfile);
        if(result != 0){
          Fluttertoast.showToast(msg: 'User Profile Updated Successfully');
          Navigator.pop(context);
        } else {
          Fluttertoast.showToast(msg: 'Something went wrong');
        }
    } else {
      result = await _databaseHelper.insertUserToDatabase(userProfile);
      if(result != 0){
        Fluttertoast.showToast(msg: 'User Profile Saved Successfully');
        setState(() {
          userProfile = UserProfile('', '', 0, 1);
          _nameController.text = userProfile.name;
          _dateController.text = userProfile.dob;
          _ageController.text = userProfile.age.toString();
        });
      } else {
        Fluttertoast.showToast(msg: 'Something went wrong');
      }
    }
  }
}