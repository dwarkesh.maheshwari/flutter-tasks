import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final height = size.height;
    final width = size.width;

    Paint paint = new Paint();
    Path path = new Path();
    path.lineTo(width, 0);
    path.lineTo(width, height * 0.2);
    path.lineTo(0, height * 0.8);
    paint.color = Colors.blue;
    canvas.drawPath(path, paint);
    // TODO: implement paint
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}