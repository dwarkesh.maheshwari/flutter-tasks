import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterapp/models/user_profile.dart';
import 'package:flutterapp/utils/database_helper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'user_profile_detail.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:csv/csv.dart';

class UsersProfileList extends StatefulWidget {
  @override
  _UsersProfileListState createState() => _UsersProfileListState();
}

class _UsersProfileListState extends State<UsersProfileList> {
  DatabaseHelper _databaseHelper = DatabaseHelper();
  List<UserProfile> userProfileList;
  int count = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataFromCsvFile();
  }

  void getDataFromCsvFile() async{
    try {
      final myData = await rootBundle.loadString('assets/text.csv');
      List<List<dynamic>> rowsAsListOfValues = const CsvToListConverter().convert(myData);
    } catch(e){
      print(e);
    }
  }
  @override
  Widget build(BuildContext context) {
    if (userProfileList == null) {
      userProfileList = <UserProfile>[];
    }
    updateListView();

    return Scaffold(
      body: getUsersListView(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          NavigateToUserProfileDetail(UserProfile('', '', 0, 1));
        },
        tooltip: "Add User",
        child: Icon(Icons.add),
      ),
    );
  }

  ListView getUsersListView() {
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
        return Card(
          color: Colors.white,
          elevation: 2.0,
          child: ListTile(
            onLongPress: () {
              showMyAlertDialog(context, this.userProfileList[position]);
//                pickImageFromGallery(this.userProfileList[position]);
            },
            title: Text(this.userProfileList[position].name),
            subtitle: Text('Birthday ' +
                this.userProfileList[position].dob +
                ', Age ' +
                this.userProfileList[position].age.toString()),
            trailing: this.userProfileList[position].gender == 1
                ? FaIcon(FontAwesomeIcons.male)
                : FaIcon(FontAwesomeIcons.female),
            leading: GestureDetector(
              onLongPress: () {
                pickImageFromGallery(this.userProfileList[position]);
              },
              onTap: () {
                print('Tapped');
                if (this.userProfileList[position].profile != null &&
                    this.userProfileList[position].profile.length != 0) {
                  showImageDialog(
                      context, this.userProfileList[position].profile);
                }
              },
              child: CircleAvatar(
                child: this.userProfileList[position].profile == null ||
                        this.userProfileList[position].profile.length == 0
                    ? FaIcon(FontAwesomeIcons.user)
                    : CircleAvatar(
                        backgroundImage: FileImage(
                            File(this.userProfileList[position].profile)),
                      ),
              ),
            ),
          ),
        );
      },
    );
  }

  showMyAlertDialog(BuildContext context, UserProfile userProfile) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Please choose any action.'),
            actions: [
              RaisedButton(
                child: Text(
                  'Edit',
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  NavigateToUserProfileDetail(userProfile);
                },
                color: Colors.blue,
              ),
              RaisedButton(
                child: Text(
                  'Delete',
                ),
                onPressed: () {
                  _databaseHelper.deleteUserProfileInDatabase(userProfile);
                  Navigator.of(context).pop();
                },
                color: Colors.redAccent,
              ),
            ],
          );
        });
  }

  void NavigateToUserProfileDetail(UserProfile userProfile) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return UserDetailsForm(userProfile);
    }));
  }

  void updateListView() {
    final Future<Database> dbFuture = _databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<UserProfile>> userProfileFuture =
          _databaseHelper.getUserProfileList();
      userProfileFuture.then((userList) {
        setState(() {
          this.userProfileList = userList;
          this.count = userList.length;
        });
      });
    });
  }

  pickImageFromGallery(UserProfile userProfile) async {
    try {
      final imageFile = await ImagePicker().getImage(source: ImageSource.gallery, maxHeight: 800, maxWidth: 800, imageQuality: 90);
      if (imageFile != null) {
//        File file = await testCompressAndGetFile(File(imageFile.path), imageFile.path);
        userProfile.profile = imageFile.path;
        int result = await _databaseHelper.updateUserProfileInDatabase(userProfile);
        if (result != 0) {
          Fluttertoast.showToast(msg: "Profile Updated sucessfully");
          setState(() {});
        }
      }
    } catch (e) {}
  }

  showImageDialog(BuildContext context, String path) {
    return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: FileImage(File(path)),
                fit: BoxFit.contain,
              )),
            ),
          );
        });
  }
  Future<File> testCompressAndGetFile(File file, String targetPath) async {
    var result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path, targetPath,
      quality: 88,
      rotate: 180,
    );

    print(file.lengthSync());
    print(result.lengthSync());

    return result;
  }
}
