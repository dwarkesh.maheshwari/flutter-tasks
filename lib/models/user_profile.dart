// The UserProfile model class is created to have a well defined structure of our form.

class UserProfile {
  int _id;
  String _name;
  String _dob;
  int _age;
  int _gender;
  String _profile;

  UserProfile(this._name, this._dob, this._age, this._gender, [this._profile]);

  UserProfile.withId(this._id, this._name, this._dob, this._age, this._gender, [this._profile]);

  int get gender => _gender;

  set gender(int value) {
    _gender = value;
  }

  int get age => _age;

  set age(int value) {
    _age = value;
  }

  String get dob => _dob;

  set dob(String value) {
    _dob = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  int get id => _id;


  String get profile => _profile;

  set profile(String value) {
    _profile = value;
  }

  //Extract a map object from UserProfile object.
  Map<String, dynamic> convertUserProfileToMap() {
    var map = Map<String, dynamic>();
    if (_id != null) {
      map['user_profile_id'] = _id;
    }
    map['user_profile_name'] = _name;
    map['user_profile_dob'] = _dob;
    map['user_profile_age'] = _age;
    map['user_profile_gender'] = _gender;
    map['user_profile_image'] = _profile;
    return map;
  }

  // Extract a UserProfile object from map object.
  UserProfile.fromMapObject(Map<String, dynamic> map) {
    this._id = map['user_profile_id'];
    this._name = map['user_profile_name'];
    this._age = map['user_profile_age'];
    this._dob = map['user_profile_dob'];
    this._gender = map['user_profile_gender'];
    this._profile = map['user_profile_image'];
  }
}
