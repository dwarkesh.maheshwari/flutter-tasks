import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutterapp/models/user_profile.dart';

class DatabaseHelper{
  
  static DatabaseHelper _databaseHelper; // Singleton DatabaseHelper
  static Database _database;
  
  DatabaseHelper._createInstance();
  
  String userProfileTable = 'user_profile_table';
  String userProfileId = 'user_profile_id';
  String userProfileName = 'user_profile_name';
  String userProfileDob = 'user_profile_dob';
  String userProfileAge = 'user_profile_age';
  String userProfileGender = 'user_profile_gender';
  String userProfileImage = 'user_profile_image';

  factory DatabaseHelper(){
    if(_databaseHelper == null) {
      _databaseHelper = DatabaseHelper._createInstance();
    }
    return _databaseHelper;
  }
  
  Future<Database> get database async{
    if(_database == null){
      _database = await initializeDatabase();
    }
    return _database;
  }
  
  Future<Database> initializeDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'users.db';
    var usersDatabase = openDatabase(path, version: 1, onCreate: _createDb);
    return usersDatabase;
  }
  
  void _createDb(Database db, int newVersion) async {
    await db.execute('''CREATE TABLE $userProfileTable($userProfileId INTEGER PRIMARY KEY AUTOINCREMENT, $userProfileName TEXT,
                              $userProfileDob TEXT, $userProfileAge INTEGER, $userProfileGender INTEGER, $userProfileImage TEXT)''');
  }
  
  
  // This function is created to fetch users profile in our database.
  Future<List<Map<String, dynamic>>> getUsersProfileMapList() async{
    Database db = await this.database;
    var result = await db.query(userProfileTable, orderBy: '$userProfileName ASC');
    return result;
  }
  
  // This function will insert a new row in user profile database.
  Future<int> insertUserToDatabase(UserProfile userProfile) async{
    Database db = await this.database;
    var result =  await db.insert(userProfileTable, userProfile.convertUserProfileToMap());
    return result;
  }
  
  //This function will update an existing row.
  Future<int> updateUserProfileInDatabase(UserProfile userProfile) async{
    Database db = await this.database;
    var result = await db.update(userProfileTable, userProfile.convertUserProfileToMap(), where: '$userProfileId = ?', whereArgs: [userProfile.id]);
    return result;
  }
  
  // This function will delete an existing row.
  Future<int> deleteUserProfileInDatabase(UserProfile userProfile) async{
    Database db = await this.database;
    var result = await db.delete(userProfileTable, where: '$userProfileId = ?', whereArgs: [userProfile.id]);
    return result;
  }
  
  // This function will get number of existing users in our database.
  Future<int> getNumberOfUserProfiles() async{
    Database db = await this.database;
    List<Map<String, dynamic>> result = await db.query(userProfileTable);
    return Sqflite.firstIntValue(result);
  }

  Future<List<UserProfile>> getUserProfileList() async {
    var userProfileMapList = await getUsersProfileMapList();
    int count = userProfileMapList.length;
    List<UserProfile> userProfileList = List<UserProfile>();

    for(int i =0; i < count; i++){
      userProfileList.add(UserProfile.fromMapObject(userProfileMapList[i]));
    }
    return userProfileList;

  }
}